# o-nakala

## Description

This code was developed based on the Nakala Notebook API (available [here](https://gitlab.huma-num.fr/huma-num-public/notebook-api-nakala)) with the aim of facilitating the use of the Nakala API for batch data deposition. It serves as a tutorial tool to assist researchers in learning how to interact with NAKALA using its API. The code simplifies and automates the process of depositing data to Nakala and interacting with the platform programmatically.
